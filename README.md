# calm-discussion-bot

Discord bot facilitating calm discussion in a Discord voice channel, allowing a speaker/admin and moderators control voice chat to maximize quality of discussion.

* the bot will allow use of different commands based on the status of the user
* by default only admin can spawn and despawn the bot in any voice channel
* once configured, any users with a moderator role can spawn and despawn a bot in a voice channel
* moderators use the bot to control conversation
* only guild(server) Admin can configure the bot

* users queue their questions into a FIFO queue, at most one at a time
* once given 'floor' they are unmuted and can speak with the current speaker and respond if need be
* after this time is done, they are once again muted

## prerequisites

* python 3 (3.6 preferably)

### python modules (use pip3 to install)

* discord.py
* python-dotenv

## features plan (updating over time)

* [ ] upon spawning in a voice channel, the bot will server mute everyone who is not admin (and once configured so, moderator)
* [ ] using any command the bot will spawn in the voice channel the user calling him is in
* [ ] bot will only accept commands if spawned
* [ ] upon user voice channel bot is in, server unmute
* [ ] upon user entering voice channel bot is in, server mute unless that user is speaker or admin (moderator?)

* [ ] default configuration in a file, runtime configuration in memory only? do configuration cleanly..
* [ ] ? user input purely through DMs (less spam) ( mods and speakers only through chat (transparency))
* [ ] user specific confirmations through DMs, not channel

* [ ] flooding/spam prevention - accept only X commands/minute from each user, after which 'cooldown' period will be triggered


### user commands

* [X] 'list' - print current queue of questions
* [X] 'ask x' - queue your question unless one is already in queue
* [X] 'yield' - end your own time earlier so that moderator can pop another question and you can queue another question
* [ ] 'cancel' - withdraw own question from queue

#### facilitator(moderator) only commands

* has access to all normal user commands
* [ ] 'answer' - print out the question first in queue and give floor to the user (un-server-mute him) for the duration of 'floor time', ping/pm the moderators and admins in this channel
* [ ] 'answerf' - same as above, but does force yield of previous user before answering question
* [ ] 'set ftime x' - set default 'floor time' (time window for a user) in seconds
* [ ] 'skip' - skip the next question in queue

#### admin only commands

* admin has access to all commnds
* [ ] 'start x' - start moderation in voice channel x
* [ ] 'stop x' - stop moderation in voice channel x
* [ ] 'set modrole x' - configure bot to treat anyone with the role X as the 'moderator' that has access to specific moderation commands
* [ ] 'set listen x' - configure bot to respond to commands in the channel X
* [ ] 'set reset' - reset all values to their defaults, starting from scratch