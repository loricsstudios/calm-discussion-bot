# Contributing rules

* do not pollute the repository by files not to be commited, if you need to work with
a tool that produces such files, make sure they're covered by `.gitignore`
* default line endings are LF, keep that in mind when working on windows before commiting your changes
* code changes accepted upon review through a merge request from your feature branch
* if your change introduces a backwards incompatible change, tag your commit
* rebase and squash your branches before requesting a merge to a minimum of commits
* one new feature = one branch
* make your commit messages clean and to the point so that reading history makes sense

## No backwards incompatible support/maintenance

* we currently don't plan on supporting older versions so hotfixes to those are not likely
* with this in mind, we will try the utmost to not produce any backwards incompatible changes so the
rate of upgrades that might break something is kept to a minimum
