# bot.py
import os
import random
import asyncio

import discord

from discord.ext import commands
# from discord.ext import VoiceChannel
from dotenv import load_dotenv
from collections import deque

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

MODERATOR_ROLE = os.getenv('MODERATOR_ROLE')
SPEAKER_ROLE = os.getenv('SPEAKER_ROLE')
TIME_WARN = 30 # 150 time in seconds when current author will be warned that his time is up soon (will be muted again)
TIME_MUTE = 10 # 15 time in seconds when question author will be muted again
EXTEND_BY = 60 # time in seconds that is added to current stopwatch when extending alloted time
NAID = 99999 # not a (valid) id

bot = commands.Bot(command_prefix='!')

queue = deque()
questions = {}
props = { 'caid' : NAID, # current author ID (who is unmuted and has floor)
           'cae' : 0, # current author extensions count
           'channel': NAID # ID of voice channel that is being moderated 
        }

@bot.event
async def on_voice_state_update(member,before,after):
    # print(f'member [{member}] voice state changed from {before} to {after}') #debug
    # if user leves channel, remove the role of 'plenum'
    if (props['channel'] != NAID and before.channel.id == props['channel'] and after.channel.id != props['channel']):
        print(f'member [{member}] left the moderated channel')
        # if member has a 'plenum' role, remove it
        # ? remove the 'speaker' role as well

# TODO add try except for the forbidden permissions
@bot.command(name='start',help='Start moderation of voice channel X (moderator only)') #requires manage_roles permission
@commands.has_role(MODERATOR_ROLE)
async def start_moderation_cmd(ctx,*,vchan: discord.VoiceChannel):
    if (props['channel'] != vchan.id):
        await ctx.send(f'Moderation of voice channel [{vchan.name}] commencing..')
        props['channel'] = vchan.id
        # set permission to speak of @everyone to false (on this voice channel)
        everyone = discord.utils.get(ctx.guild.roles, name="@everyone")
        print(f'everyone: [{everyone}]') #debug
        await vchan.set_permissions(everyone, speak=false)
        # add permission to speak of moderator, speaker and plenum roles to true (on this voice channel)
        for perm in [MODERATOR_ROLE,SPEAKER_ROLE,PLENUM_ROLE]:
            print(f'perm:[{perm}]') #debug
            permref = discord.utils.get(ctx.guild.roles, name=perm)
            print(f'permref: [{permref}]') #debug
            await vchan.set_permissions(permref, speak=true)

@bot.command(name='stop',help='Stop moderation of voice channel X (moderator only)') #requires manage_roles permission
@commands.has_role(MODERATOR_ROLE)
async def stop_moderation_cmd(ctx,*,vchan: discord.VoiceChannel):
    if (props['channel'] != NAID):
        await ctx.send(f'Moderation of voice channel [{vchan.name}] ending..')
        # set permission to speak of @everyone to defaults (on this voice channel)
        everyone = discord.utils.get(ctx.guild.roles, name="@everyone")
        await channel.set_permissions(everyone, overwrite=None)
        # set permissions settings for moderator, speaker and plenum to defaults
        for perm in [MODERATOR_ROLE,SPEAKER_ROLE,PLENUM_ROLE]:
            permref = discord.utils.get(ctx.guild.roles, name=perm)
            await vchan.set_permissions(permref, overwrite=None)
        props['channel'] = NAID


@bot.command(name='ask',help='Ask a question.')
async def ask_question_cmd(ctx,question):
    response = ''
    try:
        idx = queue.index(ctx.author.id)
        response += f'{ctx.author} already has a question in queue!\n'
    except ValueError:
        queue.append(ctx.author.id)
        questions[ctx.author.id] = f'{question}'
        response += f'{ctx.author} asked: {question}\n'
    # response += queue_string(ctx) # too spammy?
    await ctx.send(response)

@bot.command(name='answerf', help='Answer a question (speaker only), force yielding of floor.')
@commands.has_role(SPEAKER_ROLE)
async def answer_question_forced_cmd(ctx):
    await yield_floor(ctx)
    await answer_question_cmd(ctx)

@bot.command(name='answer', help='Answer a question (speaker only).')
@commands.has_role(SPEAKER_ROLE)
async def answer_question_cmd(ctx):
    response = ''
    if (len(queue) == 0):
        await ctx.send('Queue is empty, nothing to answer.')
    elif (props['caid'] != NAID):
        await ctx.send(f'User {ctx.guild.get_member(props["caid"]).name} still has floor. Use command \'answerf\' to force yielding of floor.')
    else:
        author_id = queue.pop()
        # print(f'got author_id : {author_id} of type: {type(author_id)}') #debug
        author = ctx.guild.get_member(author_id)
        # print(f'got author: {author.name}') #debug
        question = questions[author_id]
        await ctx.send(f'{author.name} asks: "{question}". Floor is yours.')
        del(questions[author_id])
        props['caid'] = author_id
        # print(f'current author: {props["caid"]}') #debug
        await author.send(f'You are now unmuted, speak about your question. Alotted time: {TIME_WARN+TIME_MUTE} seconds. You will be warned {TIME_MUTE} seconds before time is up.')
        if (props['channel'] != NAID): #if moderation is in effect
            plenum = discord.utils.get(ctx.guild.roles, name=PLENUM_ROLE)
            author.add_roles(plenum) #add ability to speak
        await stopwatch_start(ctx,author_id)

@bot.command(name='clear', help='Clear queue (moderator only).')
@commands.has_role(MODERATOR_ROLE)
async def clear_queue_cmd(ctx):
    questions = {}
    queue.clear()
    await ctx.send(f'Question queue cleared by {ctx.author}!')

@bot.command(name='list', help='List current queue.')
async def list_queue_cmd(ctx):
    await ctx.send(queue_string(ctx))

@bot.command(name='skip', help='Skip next question in queue (moderator only).')
@commands.has_role(MODERATOR_ROLE)
async def skip_question_cmd(ctx):
    response = ''
    if (len(queue)==0):
        response = 'Queue is empty, nothing to skip..'
    else:
        author_id = queue.pop()
        response = f'Skipping question from {ctx.guild.get_member(author_id).name}..'
        del(questions[author_id])
    await ctx.send(response)

@bot.command(name='cancel', help='Cancel your question in queue.')
async def cancel_question_cmd(ctx):
    try:
        idx = queue.index(ctx.author.id)
        print(f'index {idx} detected for author: {ctx.author.name}') #debug
        del(queue[idx])
        del(questions[ctx.author.id])
        await ctx.send(f'{author} withdrew own question from queue. New question can be queued now.')
    except ValueError:
        await ctx.send(f'{author} has no question in queue, nothing to cancel..')

@bot.command(name='yield', help='Yield your floor time for next user in queue.')
async def yield_floor_cmd(ctx):
    if (props['caid'] != ctx.author.id):
        await ctx.author.send('You don\'t have a floor now, so nothing to yield..')
    else:
        await yield_floor(ctx)


@bot.command(name='extend', help=f'Extend current alotted time by {EXTEND_BY} seconds (moderator only).')
@commands.has_role(MODERATOR_ROLE)
async def extend_time_cmd(ctx):
    response = ''
    if (props['caid'] == NAID):
        response = 'No question is currently being answered, no point in extending time..'
    else:
        props["cae"] += 1
        response = f'Alotted time for {ctx.guild.get_member(props["caid"])} extended by {EXTEND_BY} seconds by {ctx.author}!'
    await ctx.send(response)

async def stopwatch_start(ctx,member_id):
    print(f'warn of [{member_id} == {props["caid"]}] in {TIME_WARN} seconds') #debug
    await asyncio.sleep(TIME_WARN)
    print('first wait done')
    while (props["cae"] > 0):
        print('while loop start..')
        await asyncio.sleep(EXTEND_BY)
        if (props["cae"] > 0):
            props["cae"] -=1
        print('while loop end..')
    print('while done')
    if (props['caid'] == member_id):
        print(f'warn of {member_id} commencing..') #debug
        await dm_member(ctx,member_id,f'Last {TIME_MUTE} seconds on air!')
        print('dm sent')

    print(f'mute of [{member_id}] in {TIME_MUTE} seconds') #debug
    await asyncio.sleep(TIME_MUTE)
    while (props["cae"] > 0):
        await asyncio.sleep(EXTEND_BY)
        if (props["cae"] > 0):
            props["cae"] -=1
    if (props['caid'] == member_id):
        print(f'mute of {member_id} commencing..') #debug
        await dm_member(ctx,member_id,f'Time is up. You can now queue another question.')
        await yield_floor(ctx)

async def yield_floor(ctx):
    await ctx.send(f'Floor yielded to next question in queue..')
    if (props['channel'] != NAID): #if moderation is in effect
        author = ctx.guild.get_member(props['caid'])
        plenum = discord.utils.get(ctx.guild.roles, name=PLENUM_ROLE)
        author.remove_roles(plenum) # remove ability to speak in this moderated forum
    props['caid'] = NAID
    props["cae"] = 0

async def dm_member(ctx,member_id,msg):
    print(f'trying to pm {member_id} following message: "{msg}') #debug
    if (member_id == NAID):
        print(f'tried sending pm to ID {NAID}') #debug
        return
    print(f'fetching member from api..') #debug
    member = ctx.guild.get_member(member_id)
    print('sending dm message..') #debug
    await member.send(msg)

def queue_string(ctx):
    if (len(queue)==0):
        response = 'Question queue is empty! Ask some questions..'
    else:
        response = 'Question queue has the following items:\n'
        response += '\n * '.join([f'{ctx.guild.get_member(member_id).name}: {questions[member_id]} debug: {member_id}' for member_id in queue])
    return response

@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.errors.CheckFailure):
        await ctx.send('You do not have the correct role for this command.')

bot.run(TOKEN)