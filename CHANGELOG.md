# Changes tracking

## Untagged commits (versions 0.0.x)

* initial commit
* initial set of documentation and tracking files
* initial draft of planned features
* bot example connecting to guild, expects token to be set in env variables DISCORD_TOKEN and DISCORD_GUILD

### Bot able to respond to following commands

* `!ask {x}` (anyone)
* `!answer` ('moderator' only)
* `!clear` ('moderator' only)
* `!list` (anyone)
* `!yield` (anyone)
* `!extend` ('moderator' only)